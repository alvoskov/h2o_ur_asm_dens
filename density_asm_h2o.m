% DENSITY  ASM-H2O binary system density modeling (ASM = ammonium
% supfamate).

% Programming: Alexey Voskov
% License: MIT
function density_asm_h2o
save_to_file = true;
MH2O = 18.0153;
MASM = 114.124;
% ASM-H2O binary system experimental data (our work)
%tC mASM    mH2O    period      d,g/cm3
dat = [
15  12.604  8.402	1.03698     1.3199      1% --- 15C
15  10.515  10.563	1.033071	1.2618      1
15  7.232   11.066	1.029025	1.2018      1
15  5.44    12.601	1.025703	1.1528      1
15  3.307   13.209	1.021955	1.0977      1
15  1.55    13.509	1.01841     1.0457      1% Outlier???
15  1.5     28.623	1.016863	1.0231      1
15  0.601   29.481	1.015877	1.0087      1
%15  0       1.0000  1.01522     0.9991      1
%--------------------------------------------
25  12.604	8.402	1.036919	1.3215      2% --- 25C
25  10.515	10.563	1.033109	1.2648      2
25  7.232	11.066	1.028933	1.2030      2
25  5.44	12.601	1.025244	1.1485      2
25  3.294	13.222	1.021572	1.0945      2
25  1.55	13.509	1.018224	1.0454      2
25  1.5     28.623	1.016525	1.0206      2
25  0.601	29.481	1.015565	1.0066      2
%25	0       1.0000	0.944338	0.9970      2
%--------------------------------------------
50  15.772	5.257   1.043475	1.4278      3% --- 50C
50  14.716	6.313	1.040751	1.3870      3
50  12.604	8.402	1.036059	1.3169      3
50  10.515	10.563	1.03163     1.2511      3
50  7.232	11.066	1.027485	1.1897      3
50  5.44	12.601	1.023817	1.1356      3
50  3.307	13.209	1.020179	1.0821      3
50  1.55	13.509	1.016501	1.0283      3 % Outlier?
50  1.5     28.623	1.015328	1.0111      3
50  0.601	29.481	1.014401	0.9976      3
    ];
% ASM-H2O binary system literature data
dat_lit = [
% d,g/cm3   w(ASM)  tC  Ref    
1.0065      2       25      4%Schmelzle
1.016       4       25      4%Schmelzle
1.0255      6       25      4%Schmelzle
1.035       8       25      4%Schmelzle
1.045       10      25      4%Schmelzle
1.055       12      25      4%Schmelzle
1.065       14      25      4%Schmelzle
1.003       1.35	25      5%Mazza
1.0091      2.35	25      5%Mazza
1.0125      3.33	25      5%Mazza
1.0218      5.32	25      5%Mazza
1.031       7.6     25      5%Mazza
1.0404      9.05	25      5%Mazza
1.0504      11.13	25      5%Mazza
1.0632      13.72	25      5%Mazza
1.391       68.9	25      6%Ricci-1951
1.20512     40.04	25      7%Maurey-1963
1.14859     30.2	25      7%Maurey-1963
1.11827     24.41	25      7%Maurey-1963
1.0899      18.98	25      7%Maurey-1963
1.06117     13.32	25      7%Maurey-1963
1.04015     9.051	25      7%Maurey-1963
1.02322     5.532	25      7%Maurey-1963
1.01045     2.823	25      7%Maurey-1963
];

dat = [dat
    [dat_lit(:,3), dat_lit(:,2), 100-dat_lit(:,2), nan(size(dat_lit,1), 1), dat_lit(:,1), dat_lit(:,4)]];

% Recalculate experimental data to the suitable scale
% (molar fractions and molar volumes instead of mass fractions
% and densities)
% dat = dat(dat(:,1) == 50, :);
T = dat(:,1) + 273.15;
mASM = dat(:,2); mH2O = dat(:,3); mUR = 0;
wASM = mASM ./ (mASM + mH2O);
d = dat(:,5); % g/cm3
serID = dat(:,6); % Data series ID
Vmass = 1 ./ d; % cm3/g
nsum = wASM ./ MASM + (1 - wASM) ./ MH2O;
xASM = wASM ./ MASM ./ nsum;
Vm = Vmass ./ nsum; % molar volume, cm3/mol

% ----- Print the table with experimental densities
ii = dat(:,6) == 1 | dat(:,6) == 2 | dat(:,6) == 3;
xv = unique(xASM(ii));
dv = nan(numel(xv), 3);
for i = 1:numel(xv)
    for j = 1:3
        ind = find(dat(:,6) == j & xASM == xv(i));
        if ~isempty(ind)
            dv(i,j) = dat(ind,5);
        end
    end    
end
fprintf('%7s & %7s & %7s & %7s\n', 'xASM', '15C', '25C', '50C');
fprintf('%7.5f & %7.4f & %7.4f & %7.4f \\\\\n', [xv, dv]');

% ----- Find model parameters
beta0 = [0 0 0 0 0 0];
[beta, res, J] = nlinfit([xASM T], Vm, @Vm_curve, beta0);
disp('Before rounding');
[~, ndigits, beta_rounded] = display_parameters(beta, res, J, 5);
disp(ndigits);
[ndigits, beta_rounded] = round_parameters(beta, @(b) Vm_curve(b, [xASM, T]) - Vm);
res_rounded = Vm - Vm_curve(beta_rounded, [xASM T]);
% ----- Show model parameters
disp('After rounding');
display_parameters(beta_rounded, res_rounded, J);
beta = beta_rounded;
Vm_print_b(1, beta_rounded); % 1 is stdout

%beta = [6.40515566e+01 -1.12488336e-03 4.89955241e-01 -7.91038006e-02 3.18433788e+00 1.19371509e-02];
% ----- Show the results in the form of figures
close all;
% Draw figure with extrapolation of densities up to 370 K
% Experimental points with density anomaly are shown on the graph
% a) Build big plot
figure;
hold on;
xv = [0 0.09352 0.13580 0.19146];
lines = {'b--', 'b-', 'k-', 'r-'};
markers = {'rh', 'bv', 'kd', 'r^'};
% Experimental points
for ii = 1:numel(xv)
    xi = xv(ii);
    inds = abs(xi - xASM) <= 1e-3;
    if any(inds)
        Vmser = Vm(inds);
        Tser  = T(inds);
        plot(Tser, Vmser, markers{ii}, 'LineWidth', 2);
    end
end
% Theoretical lines
for ii = 1:numel(xv);
    xi = xv(ii);
    TASMextr = 280:1:370;
    xASMextr = xi * ones(size(TASMextr));
    [Vmextr] = Vm_curve(beta, [xASMextr', TASMextr']);
    plot(TASMextr, Vmextr, lines{ii}, 'LineWidth', 2);
end
hold off;
xlabel('$T$ / K', 'Interpreter', 'LaTeX', 'FontSize', 14);
ylabel('$V_\mathrm{m}$ / $\mathrm{cm^3\cdot{mol}^{-1}}$', 'Interpreter', 'LaTeX', 'FontSize', 14);
set(gca, 'FontSize', 12);
axis([280 370 17.5 30]);
box on;
% b) Build subplot for T = 298.15 K
axes('Position', [0.664, 0.229, 0.216, 0.220]);
hold on;
ii = 3;
xi = xv(ii);
inds = abs(xi - xASM) <= 1e-3;
plot(T(inds), Vm(inds), markers{ii}, 'LineWidth', 2);
TASMextr = 285:1:325;
xASMextr = xi * ones(size(TASMextr));
Vmextr = Vm_curve(beta, [xASMextr', TASMextr']);
plot(TASMextr, Vmextr, lines{ii}, 'LineWidth', 2);
hold off;
box on;
%axis([285 325 floor(min(Vmextr)) ceil(max(Vmextr))]);
axis([285 325 24.5 25.0]);
set(gca, 'XTick', [290 300 310 320]);
set(gca, 'YTick', [24.5 24.6 24.7 24.8 24.9 25.0]);
if save_to_file
    save_fig_to_files('Saving Fig.X(Vm(T) for H2O-ASM) to file', 'figX_VmT');
end

% ----- Type tabulated densities
fprintf('Tabulated densities\n');
xASMextr = unique(sort([0:0.01:0.25 xASM(:)']));
for ii = 1:numel(xASMextr)
    tbl = [xASMextr];
    for Tj = 288.15:5:323.15        
        TASMextr = Tj*ones(size(xASMextr));
        Vmextr = Vm_curve(beta, [xASMextr', TASMextr']);
        dextr = (xASMextr * MASM + (1 - xASMextr) * MH2O)./Vmextr(:)';
        %tbl = [tbl; Vmextr(:)'];        
        tbl = [tbl; dextr];
    end    
end
fprintf([repmat('%6.2f ', 1, size(tbl, 1)), '\n'], [NaN, 288.15:5:323.15]);
fprintf([repmat('%6.4f ', 1, size(tbl, 1)), '\n'],  tbl);
fprintf('\n');

% ----- Type information about partial volumes
fprintf('Partial volume; T=298.15 K\n');
xASMt = (0:0.05:1.0)';
[Vmt, Vmlint, VmSRt, VmDHt] = Vm_curve(beta, [xASMt, 298.15*ones(size(xASMt))]);
for i = 1:numel(xASMt)
    fprintf('x=%10.5f; %10.5f %10.5f %10.5f %10.5f | %10.5f\n', ...
        xASMt(i), Vmt(i), Vmlint(i), VmSRt(i), VmDHt(i), ...
        (VmSRt(i) + VmDHt(i))./Vmt(i)...
    );
end

% ----- Show the results in the form of figures (ending)
markers = {'bo', 'kd', 'rv', 'k+', 'mx', 'ks', 'k^', 'mh', 'mp'};
% Figure with model residuals
figure;
hold on;
for i = 1:numel(markers)
    ii = serID == i;
    plot(xASM(ii), 100*res(ii)./Vm(ii), markers{i}, 'LineWidth', 2);
end
ax = axis;
plot(ax(1:2), [0 0], 'k-', 'LineWidth', 2);
sy = sqrt(sumsqr(res*100./Vm) ./ (numel(Vm) - numel(beta)));
plot(ax(1:2), [sy sy], 'k--', 'LineWidth', 2);
plot(ax(1:2), [-sy -sy], 'k--', 'LineWidth', 2);
hold off;
if ~save_to_file
    legend('This work (15\circ{C})', ...
        'This work (25\circ{C})', ...
        'This work (50\circ{C})', ...
        'Schmelzle (25\circ{C})', 'Mazza (25\circ{C})', ...
        'Ricci-1951 (25\circ{C})', 'Maurey-1963 (25\circ{C})',...
        'Location', 'Best');
end
set(gca, 'FontSize', 12);
xlabel('$x_\mathrm{NH_2SO_3NH_4}$', 'FontSize', 14, 'Interpreter', 'latex');
ylabel('$\left(V_\mathrm{m} - V_\mathrm{m}^\mathrm{calc}\right)/V_\mathrm{m}\cdot 100$', ...
    'FontSize', 14, 'Interpreter', 'latex');
box on;
dy = max(1, max(abs(res*100./Vm)));
ax(3:4) = [-dy dy];
axis(ax);
if save_to_file
    save_fig_to_files('Saving Fig.3(H2O-ASM) to file', 'fig3_h2o_asm');
end

% Figure with partial molar volumes at 298.15 and different concentartions
figure;
hold on;
markers = {'ro', 'b+', 'bx', 'bs', 'bd'};
xv = 0.001:0.001:0.3;
Vmv = Vm_curve(beta, [xv' ones(size(xv'))*298.15]);
plot((xv), Vmv, 'k-', 'LineWidth', 2);
ids = [2 4:7]; % 25C
for i = 1:numel(ids)
    ii = serID == ids(i);
    plot((xASM(ii)), Vm(ii), markers{i}, 'LineWidth', 2);
end
box on;
hold off;
set(gca, 'FontSize', 12);
xlabel('$x_\mathrm{NH_2SO_3NH_4}$', 'FontSize', 14, 'Interpreter', 'latex');
ylabel('$V_\mathrm{m} / \mathrm{cm^3\cdot{mol}^{-1}}$', 'FontSize', 14, 'Interpreter', 'latex');
% Subplot
axes('Position', [0.25, 0.6, 0.3, 0.3]);
hold on;
for i = 1:numel(ids)
    ii = serID == ids(i);
    plot((xASM(ii)), Vm(ii), markers{i}, 'LineWidth', 2);
end
plot((xv), Vmv, 'k-', 'LineWidth', 2);
axis([0 0.03 18 19.5]);
hold off;
box on;
if save_to_file
    save_fig_to_files('Saving Fig.2(H2O-ASM) to file', 'fig2_h2o_asm_Vm');
end
end


function [Vm, Vm_lin, Vm_SR, Vm_DH] = Vm_curve(b,xT)
x = xT(:,1);
T = xT(:,2);
Vm_H2O = calc_Vm_H2O(T, 100); % and 100 KPa (1 bar)
Vm_lin = Vm_H2O.*(1-x) + (b(1)).*x;
xi = x ./ (1 + x);
Wij = b(2) + b(3)./T;
Uij = b(4) + b(5)./T + b(6)*log(T);
Vm_SR = (1+x).*Vex_S_bin(Wij, Uij, T, xi);
Vm_DH = (1+x).*Vex_DH(xi, T);
Vm = Vm_lin + Vm_SR + Vm_DH;
end

function Vm_print_b(fp, b)
fprintf(fp, 'Vm_ASM = (%.6e);\n', b(1));
fprintf(fp, 'Wij = (%.6e) + (%.6e)./T;\n', b(2:3));
fprintf(fp, 'Uij = (%.6e) + (%.6e)./T + (%.6e).*log(T);\n', b(4:6));
end
