% SUMSQR  Sum of squares (to replace function from
% Neural Network Toolbox)

% (C) 2016 Alexey Voskov
% License: MIT
function [s, n] = sumsqr(x)
ii = ~isnan(x) & ~isinf(x);
s = sum(x(ii).^2);
n = sum(ii(:));
end
