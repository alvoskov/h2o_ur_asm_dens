% CALC_AV  Formula for Av coefficient for Debye-Huckel model
% for T = 273.15-373.15 K and p = 0.1MPa (based on data from [1])
% [1] Archer D.G., Wang P. The dielectric constant of water and
% Debye-Huckel limiting law slopes // J. Phys. Chem. Ref. Data. 1990. V.19,
% N 2, 371-411.
%
% Usage:
%   [Av, Avx] = calc_Av(T)
% Inputs:
%   T -- temperature, K
% Outputs:
%   Av -- Debye-Huckel limiting law slope for V, cm3*kg^0.5*mol^-1.5
%   Avx -- Debye-Huckel limiting law slope for V, cm3/mol
%
% See also APPROX_AV

% Programming: Alexey Voskov
% License: MIT
function [Av, Avx] = calc_Av(T)
Av = -2268.4013 - 7.4554933*T + 2.8082151e-03*T.^2 + 230.58375*T.^0.5 + 78103.548./T;
Mw = 18.0153; %M(H2O), g/mol
Avx = Av * sqrt(1000./Mw);
end