% DENSITY_WATER  Describes experimental data about water density
% (molar volume) using data from IAPWS-97 and nonlinear regression
% References:
%   http://dx.doi.org/10.1063/1.1461829
%   J. Phys. Chem. Ref. Data. 2002. V.31(2). P.387-535.
function density_water
% Experimental data
dat = [
    280     999.911     101.325
    285     999.517     101.325
    290     998.804     101.325
    295     997.807     101.325
    300     996.557     101.325
    305     995.076     101.325
    310     993.384     101.325
    315     991.496     101.325
    320     989.427     101.325
    325     987.187     101.325
    330     984.787     101.325
    335     982.234     101.325
    340     979.536     101.325
    345     976.699     101.325
    350     973.728     101.325    
    355     970.629     101.325
    360     967.404     101.325
    365     964.057     101.325
    370     960.592     101.325
    
    280     999.983     250
    285     999.588     250
    290     998.873     250
    295     997.875     250
    300     996.624     250
    305     995.142     250
    310     993.449     250
    315     991.561     250
    320     989.492     250
    325     987.252     250
    330     984.852     250
    335     982.299     250
    340     979.601     250
    345     976.765     250
    350     973.795     250
    355     970.696     250
    360     967.471     250
    365     964.126     250
    370     960.661     250
    
    280     1000.10     500
    285     999.706     500
    290     998.989     500
    295     997.989     500
    300     996.736     500
    305     995.253     500
    310     993.559     500
    315     991.671     500
    320     989.601     500
    325     987.361     500
    330     984.961     500
    335     982.409     500
    340     979.711     500
    345     976.876     500
    350     973.906     500
    355     970.808     500
    360     967.585     500
    365     964.241     500
    370     960.778     500
    ];
% Calculate molar volume (M from IAPWS-97 is used)
T = dat(:,1);
d = dat(:,2);
p = dat(:,3) ./ 100; % Transform pressure to BAR
Vm = 1000./d * 18.015268;
% Regression
func = @(b,Tp) b(1) + b(2)*T + b(3)./T(:,1) + b(4) * (Tp(:,2) - 1);
[beta, res, J] = nlinfit([T p],Vm, func, [18 0 0 0]);
ci = nlparci(beta, res, 'Jacobian', J);
display('Unrounded coefficients');
[~,ndigits, beta_rounded] = display_parameters(beta, res, J, 2);
ndigits
display('Rounded coefficients');
display_parameters(beta_rounded, res, J);
% Show the results
Tv = 275:1:373;
close all;
%plot(T,Vm, 'bo', Tv, func(beta, Tv), 'k-', 'LineWidth', 2);
%xlabel('{\it{T}}, K', 'FontSize', 12);
%ylabel('{\it{V}}_m, cm^3\cdot{mol}^{-1}', 'FontSize', 12);
figure;
plot(T, 100*res./Vm, 'bo', 'LineWidth', 2);
xlabel('{\it{T}}, K', 'FontSize', 12);
ylabel('({\it{V}}_m - {\it{V}}_m^{calc})/{\it{V}}_m, %', 'FontSize', 12);
title('Before coefficient rounding');

figure;
res_rounded = Vm - func(beta_rounded, [T p]);
plot(T, 100*res_rounded./Vm, 'bo', 'LineWidth', 2);
xlabel('{\it{T}}, K', 'FontSize', 12);
ylabel('({\it{V}}_m - {\it{V}}_m^{calc})/{\it{V}}_m, %', 'FontSize', 12);
title('After coefficient rounding');


end