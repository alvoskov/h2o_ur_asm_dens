% GETR  Returns universal gas constant in J/(mol*K)
% R value is taken from CODATA 2014 that is accessible at:
% [1] http://physics.nist.gov/constants
% [2] http://dx.doi.org/10.5281/zenodo.22826
%
% Usage:
%   R = getR();
%
% See also: compProp

% Programming: Alexey Voskov
% License: MIT
function R = getR()
    R = 8.3144598; %(48); J/(mol*K); rel.std.uncert = 5.7e-7
end