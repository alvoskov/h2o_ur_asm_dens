% APPROX_AV  Av coefficient for Debye-Huckel model approximation
% for T = 273.15-373.15 K and p = 0.1MPa
%
% Data are taken from:
% [1] Archer D.G., Wang P. The dielectric constant of water and
% Debye-Huckel limiting law slopes // J. Phys. Chem. Ref. Data. 1990. V.19,
% N 2, 371-411.

% Programming: Alexey Voskov
% License: MIT
function approx_Av
close all;
dat = [ % T,K  Av(p=0.1MPa)
    %263.15  1.4769
    %268.15  1.4801
    273.15  1.5066
    278.15  1.5498
    283.15  1.6057
    288.15  1.6720
    293.15  1.7472
    298.15  1.8305
    303.15  1.9214
    308.15  2.0198
    313.15  2.1256
    318.15  2.2389
    323.15  2.3601
    328.15  2.4894
    333.15  2.6273
    338.15  2.7743
    343.15  2.9309
    348.15  3.0979
    353.15  3.2759
    358.15  3.4657
    363.15  3.6683
    368.15  3.8845
    373.15  4.1156
    ];

T = dat(:,1);
Av = dat(:,2);

% Nonlinear regression
T0 = 300;
Av_func = @(b,T) b(1) + b(2)*T./T0 + b(3).*T.^2./T0.^2 + b(4).*T.^0.5 + b(5)./T.*T0;
X = [ones(size(T)), T/T0, T.^2./T0.^2, T.^0.5, T0./T];
b = (X\Av)'
res = Av_func(b,T) - Av;
sigma2 = sumsqr(res) ./ (numel(T) - numel(b));
fprintf('sigma = %.3e\n', sqrt(sigma2));
% Round coefficients
sb = diag(sqrt(sigma2 * inv(X'*X)))
db = sb * tinv(0.975, numel(T) - numel(b))
[db, ndigits, b_rounded] = display_parameters(b, res, X, 6);
sigma2_round = sumsqr(Av - Av_func(b_rounded, T)) ./ (numel(T) - numel(b));
fprintf('sigma(after round.) = %.3e\n', sqrt(sigma2_round));
display_parameters(b_rounded, res, X);
% Prepared expressions
Av_fix = -2268.4013 - 7.4554933*T + 2.8082151e-03*T.^2 + 230.58375*T.^0.5 + 78103.548./T;
sigma2_fix = sumsqr(Av_fix - Av)./(numel(T) - numel(b));
fprintf('sigma(fix.expr.) = %.3e\n', sqrt(sigma2_fix));
% Show the final expression
fprintf('\nAv = (%.10e) + (%.10e)*T + (%.10e)*T.^2 + (%.10e)*T.^0.5 + (%.10e)./T\n\n',...
    b_rounded .* [1, 1./T0, 1./T0.^2, 1, T0]);
% Show graph
Tv = 263:1:374;
plot(Tv, Av_func(b, Tv), 'k-', T, Av, 'bo', 'LineWidth', 2);
% Show table
Av_calc = Av_func(b_rounded,T);
for i = 1:numel(T)    
    fprintf('%.2f %.4f %.4f %.4f\n', ...
        T(i), Av(i), Av_calc(i), Av(i) - Av_calc(i))
end
end