% Shows model parameters on the screen
% Inputs:
%   beta -- model parameters
%   res  -- residuals vector
%   J    -- Jacobian
%   extra_digits -- number of extra digits

% Programming: Alexey Voskov
% License: MIT
function [dbeta, ndigits, beta_rounded] = display_parameters(beta, res, J, extra_digits)
if nargin == 3
    extra_digits = 0;
end
fprintf('Model standard deviation: %.3e\n', calc_sy(res, numel(beta)));
dbeta = calc_da(beta, res, J);
fprintf('%3s | %15s %8s | %4s\n', '#', 'beta', 'dbeta', 'db/b(%)');
for i = 1:numel(beta)
    fprintf('%3d | %15.8e %8.2e | %4.1f\n', i, beta(i), dbeta(i), ...
        abs(100*dbeta(i)./beta(i)));
end
ndigits = ceil(-log10(abs(dbeta(:)./beta(:))));

beta_rounded = nan(size(beta));
for i = 1:numel(beta)
    n = ndigits(i) + extra_digits;
    if n == 0
        n = 1;
    end
    beta_rounded(i) = round(beta(i), n, 'significant');
end
end

function [da, sa] = calc_da(a, res, J)
res = res(:);
f = numel(res) - numel(a);
sigma2 = (res'*res) ./ f; t = tinv(0.975, f);
c = full(sigma2*inv(J'*J));
sa = sqrt(diag(c));
da = sa * t;
end

function sy = calc_sy(res, k)
    sy = sqrt(sumsqr(res) ./ (numel(res) - k));
end
