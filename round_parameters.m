% ROUND_PARAMETERS  Rounds least-squares parameters taking into account
% its correlation with each other using the method of step-by-step elimination
% of decimal digits of the parameters.
%
% Usage:
%   [ndigits, beta_rnd, res, J] = round_parameters(beta, func)
%
% Inputs:
%   beta -- model parameters
%   func -- anonymous function @(beta) (...) that returns residuals vector
% Outputs:
%   ndigits -- number of significant decimal digits for each parameter
%   beta_rnd -- rounded model parameters
%   res  -- residuals vector
%   J    -- Jacobian

% Programming: Alexey Voskov
% License: MIT
function [ndigits, beta_rnd, res, J] = round_parameters(beta, func)
[res, J] = calc_res_J(func, beta);
sy0 = calc_sy(res, numel(beta));
ndigits = 20*ones(size(beta));

% Calculate new value
sy = sy0;
while true
    sy_vec = nan(size(beta));
    for i = 1:numel(beta)
        nd = ndigits; nd(i) = nd(i) - 1;
        beta_rnd = round_beta(beta, nd);
        res = calc_res_J(func, beta_rnd);
        sy_vec(i) = calc_sy(res, numel(beta));
    end
    sy_vec(ndigits == 2) = 100*sy0;
    [sy_new, ind] = min(sy_vec);
    if sy_new / sy0 < 1.001
        ndigits(ind) = ndigits(ind) - 1;
        sy = sy_new;        
    else
        break;
    end
end

beta_rnd = round_beta(beta, ndigits);
[res, J] = calc_res_J(func, beta_rnd);
end

function beta_rnd = round_beta(beta, ndigits)
beta_rnd = nan(size(beta));
for i = 1:numel(beta)
    beta_rnd(i) = round(beta(i), ndigits(i), 'significant');
end
end

function [res, J] = calc_res_J(func, beta)
res = func(beta);
res = res(:);
if nargout == 2
    J = nan(numel(res), numel(beta));
    dx = 1e-6;
    for i = 1:numel(beta)
        betaB = beta; betaB(i) = betaB(i) + dx;
        betaA = beta; betaA(i) = betaA(i) - dx;
        resB = func(betaB); resB = resB(:);
        resA = func(betaA); resA = resA(:);
        J(:,i) = (resB - resA)./(2*dx);
    end
end
end

function sy = calc_sy(res, k)
    sy = sqrt(sum(res.^2) ./ (numel(res) - k));
end
