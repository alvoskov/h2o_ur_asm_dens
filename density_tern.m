% DENSITY_TERN  Optimizes the parameters of model for density in
% ASM-UR-H2O system (ASM = ammonium sulfamate, UR = urea)
function density_tern
save_to_file = false;
MH2O = 18.0153;
MASM = 114.124;
MUR = 60.0553;
function Vm = d_to_Vm(d, wUR, wH2O)
    Vmass = 1./d; % cm3/g
    wASM = 1 - wUR - wH2O;

    nsum_ = wUR ./ MUR + wH2O ./ MH2O + wASM ./ MASM;
    Vm = Vmass ./ nsum_; % molar volume, cm3/mol
end

% Experimental data (our work)
%No d,g/cm3     masses
%	15C         25C         50C         mASM    mUr     mH2O
dat = [
1	1.059394009	1.055545857	1.04401213	3.491	5.511	45.237
2	1.142632734	1.137497096	1.123827761	4.884	7.713	20.293
3	1.216057674	1.210140327	1.195329777	5.234	8.262	10.563
4	1.068543114	1.064444311	1.053684459	3.538	1.862	27.376
5	1.170852836	1.166228158	1.150389391	5.896	3.104	13.913
6	1.179336219	1.17356559	1.160547676	5.894	3.1     13.062 % Former 8
7	1.261747233	1.257678978	1.242194642	8.844	4.656	9.94   % Former 6
8	1.082847146	1.078816964	1.068007075	3.826	0.673	20.435 % Former 7
9	1.258982242	1.261669231	1.248577888	11.479	2.015	11.983];

% Calculate mass fractions
wsum = dat(:,5) + dat(:,6) + dat(:,7);
wUR = dat(:,6) ./ wsum;
wH2O = dat(:,7) ./ wsum;

% Calculate molar fractions
nASM = dat(:,5) ./ MASM;
nUR = dat(:,6) ./ MUR;
nH2O = dat(:,7) ./ MH2O;
nsum = nASM + nUR + nH2O;
xASM = nASM ./ nsum;
xUR = nUR ./ nsum;
xH2O = nH2O ./ nsum;
% Calculate molar volumes
d15 = dat(:,2); Vm15 = d_to_Vm(d15, wUR, wH2O);
d25 = dat(:,3); Vm25 = d_to_Vm(d25, wUR, wH2O);
d50 = dat(:,4); Vm50 = d_to_Vm(d50, wUR, wH2O);
% Show data for the table with experimental data
fprintf('%3s & %7s & %7s & %7s & %7s & %7s\\\\\n', ...
    'No', '$x_2$', '$x_3$', '288.15 K', '298.15 K', '323.15 K');
fprintf('%3d & %7.5f & %7.5f & %7.4f & %7.4f & %7.4f \\\\\n', ...
    [dat(:,1), xUR, xASM, d15, d25, d50]');
% Show vectors with experimental data
fprintf('xUR:  '); fprintf('%.6f, ', xUR); fprintf('\n');
fprintf('xASM: '); fprintf('%.6f, ', xASM); fprintf('\n');


% Prepare data series and optimize ternary parameters
Vm = [Vm15; Vm25; Vm50];
Tv = [15*ones(size(Vm15)); 25*ones(size(Vm15)); 50*ones(size(Vm15))] + 273.15;
opt = optimset('Display', 'iter');
beta0 = [0 0 0];
[beta,~,res,~,~,~,J] = lsqnonlin(@(p) Vmfunc(Tv, repmat(xUR, 3, 1), repmat(xH2O, 3, 1), p) - Vm, beta0, [], [], opt);

display_parameters(beta, res, J);
func = @(b) Vm - Vmfunc(Tv, repmat(xUR, 3, 1), repmat(xH2O, 3, 1), b);
[ndigits, beta_rounded, res_rounded, J] = round_parameters(beta, func);
disp(ndigits);
display_parameters(beta_rounded, res_rounded, J);
beta = beta_rounded;
Vm_calc = Vmfunc(Tv, repmat(xUR, 3, 1), repmat(xH2O, 3, 1), beta);

% Test the fraction of ternary term
[xURv, xH2Ov] = meshgrid(0.05:0.05:0.95);
ii = xURv + xH2Ov <= 1;
xURv = xURv(ii);
xH2Ov = xH2Ov(ii);


[Vmv, Vmternv] = Vmfunc(298.15, xURv, xH2Ov, beta);
fprintf('max(Vmternv/Vmv) = %.3f %\n', max(abs(Vmternv./Vmv))* 100);

% Show the result
figure;
hold on;
Tlist = [15 25 50] + 273.15;
markers = {'bo', 'kd', 'rv'};
res = [];
for i = 1:numel(Tlist);
    ii = Tv == Tlist(i);
    Vm_ = Vm(ii);
    Vm_calc_ = Vm_calc(ii);
    res_ = (Vm_ - Vm_calc_)./Vm_ * 100;    
    plot(1:numel(Vm_), res_, markers{i}, 'LineWidth', 2);
    res = [res; res_(:)];
end
plot([1 numel(Vm_)], [0 0], 'k-', 'LineWidth', 2);
sy = sqrt(sumsqr(res) ./ (numel(Vm) - numel(beta)));
plot([1 numel(Vm_)], [sy sy], 'k--', 'LineWidth', 2);
plot([1 numel(Vm_)], [-sy -sy], 'k--', 'LineWidth', 2);
hold off;
dy = max(0.5, 2*sy);
axis([1 numel(Vm_) -dy dy]);
xlabel('$n$', 'FontSize', 14, 'Interpreter', 'latex');
ylabel('$\left(V_\mathrm{m} - V_\mathrm{m}^\mathrm{calc}\right)/V_\mathrm{m}\cdot 100$', 'FontSize', 14, 'Interpreter', 'latex');
if ~save_to_file
    h = legend('15^\circ{C}', '25^\circ{C}', '50^\circ{C}', ...
        'Location', 'Best');
    set(h, 'FontSize', 12);
end
set(gca,'fontsize',12);
box on
%set(gcf, 'Units', 'Centimeters');
%set(gcf, 'Position', [0 0 10 8]);
%set(gcf, 'PaperPositionMode', 'auto');
if save_to_file
    save_fig_to_files('Saving Fig.4(TERNARY) to file', 'fig4_ternary');
end
% Show the density surface
%close all;
figure;
[xURm, xASMm] = meshgrid([0.001 0.025:0.025:(1-0.025) 0.999]);
ii = xURm + xASMm <= 1;
xURm(~ii) = NaN;
xASMm(~ii) = NaN;
xH2Om = 1 - xURm - xASMm;
xH2Om(xH2Om < eps) = eps;
Vmv = Vmfunc(298.15, reshape(xURm, numel(xURm), 1), reshape(xH2Om, numel(xH2Om), 1), beta);
Vmm = reshape(Vmv, size(xURm));
surf(xASMm, xURm, Vmm);
title('Vm of ternary solution at 298.15 K');
xlabel('xASM');
ylabel('xUR');
end

function [Vm, V_tern] = Vmfunc(T, xUR, xH2O, param)
R = getR() * 10;
Vm_H2O = calc_Vm_H2O(T);
% H2O-ASM system
Vm_ASM = (6.427000e+01);
W_WI = (5.640000e-04) + (-1.760000e-01)./T;
U_WI = (-3.667300e-01) + (1.725100e+01)./T + (5.427650e-02).*log(T);
% Old (in not corrected proof)
%Vm_ASM = (6.405156e+01);
%W_WI = (-1.124880e-03) + (4.899550e-01)./T;
%U_WI = (-7.910380e-02) + (3.184340e+00)./T + (1.193720e-02).*log(T);
% H2O-UR system
p = 100; % 1 bar
Vm_UR = (2.470200e+01) + (4.840400e-01)*T + (-7.146600e-02)*T.*log(T) + (-7.039500e-02) .* (p./100 - 1);
W_WUR = (5.626000e-04) + (-2.047200e-01)./T;
% Concentration of components (x) and species (y)
xASM = 1 - xUR - xH2O;
yI = xASM./(1 + xASM);
yUR = xUR./(1 + xASM);
yW  = xH2O./(1 + xASM);
% Ternary system
Vm_lin = xASM.*Vm_ASM  + xUR.*Vm_UR + xH2O.*Vm_H2O;
Vm_bin = (1 + xASM).*( ...
    Vex_S_bin(W_WI,U_WI,T,yI,yW) + ... % H2O-ASM system contribution
    0*Vex_S_bin(0, 0, T, yI, yUR) + ... % UR-ASM system contribution
    R*T .* yW.*yUR.*(W_WUR) + ... % H2O-UR system contribution    
    Vex_DH(yI, T) ...
    );
Vm = Vm_lin + Vm_bin;
V_tern = (1 + xASM).*( R*T.* 2.*yW.*yUR.*yI.*(param(1)./T + param(2) + param(3).*log(T)));
Vm = Vm + V_tern;
end