% SAVE_FIG_TO_FILE Saves the current figures to EPS, PDF and 300 DPI PNG
% files (in /output folder)
%
% Usage:
%   save_fig_to_files(msg, filename, type)
% Inputs:
%   msg
%   filename
%   type -- 'vector' or 'bitmap'

% Programming: Alexey Voskov
% License: MIT
function save_fig_to_files(msg, filename, type)
if nargin < 3
    type = 'vector';
end
disp(msg);
if strcmp(type, 'vector')
    print(sprintf('output/%s_raw.eps', filename), '-depsc');    
    print(sprintf('output/%s.png', filename), '-dpng', '-r300');
    dos(sprintf('eps2eps -dNOCACHE output/%s_raw.eps output/%s.eps', filename, filename));
    dos(sprintf('epstopdf output/%s.eps', filename));
elseif strcmp(type, 'bitmap');
    print(sprintf('output/%s.png', filename), '-dpng', '-r600');
else
    error('Unknown type (must be ''vector'' or ''bitmap''');
end
end