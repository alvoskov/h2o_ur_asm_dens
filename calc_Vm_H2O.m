% CALC_VM_H2O  Calculates molar volume of water using the expression
% based on IAPWS-IF97 data.
%
% Usage;
%   Vm_H2O = calc_Vm_H2O(T, p)
%
% Inputs:
%   T -- T, K
%   p -- p, kPa (optional; default is 101.325)
% Outputs:
%   Vm_H2O -- molar volume of water, cm3/mol
%

% Programming: Alexey Voskov
% License: MIT
function Vm_H2O = calc_Vm_H2O(T, p)
if nargin == 1
    p = 101.325;
end
Vm_H2O = 1.541 + 3.0009e-2*T + 2.2603e3./T - 8.36e-4*(p - 100)./100; 
end