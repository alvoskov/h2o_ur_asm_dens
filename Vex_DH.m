% VEX_DH  Debye-Huckel contribution to the excess volume in the
% Pitzer-Simonson-Clegg model for MX-S1-S2 system (1-1 electrolyte).
%
% Usage:
%   Vm_DH = Vex_DH(xion, T)
%
% See also CALC_AV

% Programming: Alexey Voskov
% License: MIT
function Vm_DH = Vex_DH(xion, T)
b = 14.9;
Ix = xion;
[~, Avx] = calc_Av(T);
Vm_DH = Avx.*Ix./b.*log( (1 + b*sqrt(Ix)) ./ (1 + b*sqrt(0.5)) );
end