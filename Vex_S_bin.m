% VEX_S_BIN  Short-range term from Pitzer-Simonson-Clegg model of excess
% volume. It uses the next expression that can be used even inside the
% expression for ternary system:
%   V = R*T*( 2*xion*xs*[Wis + 2/3*Uis*(1 - xion + xs)])
%
% Note: 
%  1) 2*xion + xs = 1 (MX-S electrolyte solution is modelled)
%  2) V is normed per mol of SPECIES (not non-dissociated components)
%
% Usage:
%   V = Vex_S_bin(W, U, T, xion, xs)
%
% See also GETR

% Programming: Alexey Voskov
% License: MIT
function V = Vex_S_bin(W, U, T, xion, xs)
R = getR() * 10;
if nargin == 4
    xs = 1 - 2*xion;
end
if any(xion(:) < 0 | xion(:) > 1)
    error('Invalid xion value');
end
if any(xs(:) < 0 | xs(:) > 1)
    error('Invalid xs value');
end

%V = R*T.*2.*xion.*xs.*(W + 2/3*U.*(1 + xs - xion)); %OLD!
V = R*T.*2.*xion.*xs.*(W + 2/3*U.*(1 - xs + xion));
end